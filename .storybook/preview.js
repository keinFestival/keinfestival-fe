import React, { useState, useEffect } from 'react'
import { ThemeProvider } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import { darkTheme, lightTheme } from '/styles/theme';
import ColorModeContext from "/styles/ColorModeContext";
import { staticAppContext } from "/lib/static/appContext";
import AppContext from "/lib/context/AppContext";
import addons from '@storybook/addons';

const channel = addons.getChannel();

// Set up a theme provider for Storybook
const ThemeProviderView = (Component, pageProps) => {
  const [darkMode, setDarkMode] = useState(false);

  useEffect(() => {
    // listen to DARK_MODE event
    channel.on('DARK_MODE', setDarkMode);
    return () => channel.off('DARK_MODE', setDarkMode);
  }, [channel]);

  const _setDarkMode = (newmode) => {
    setDarkMode(newmode);
  };

  return (
    <AppContext.Provider value={staticAppContext}>
      <ColorModeContext.Provider value={{ darkMode, setDarkMode: _setDarkMode }} >
        <ThemeProvider theme={darkMode ? darkTheme : lightTheme} >
          <CssBaseline />
          <Component {...pageProps} />
        </ThemeProvider>
      </ColorModeContext.Provider>
    </AppContext.Provider>
  );
};

export const decorators = [ThemeProviderView];

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
  darkMode: {
    // current: 'dark',
    classTarget: 'html',
    stylePreview: true
  }
}
