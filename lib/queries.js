import { gql } from "@apollo/client";
// import gql from "graphql-tag";
import { IMAGE_FIELDS, META_FIELDS, AUTHOR_FIELDS } from "/lib/fragments";

const queryGlobal = gql`
	query Global {
		global {
			data {
				attributes {
					siteName
					defaultSeo {
						metaTitle
						metaDescription
						shareImage {
							...imageFields
						}
					}
					favicon {
						...imageFields
					}
				}
			}
		}
	}
`;

export const articles = gql`
	query Articles {
		articles {
			data {
				attributes {
					title
				}
			}
		}
	}
`;

export const articles2 = gql`
	query Articles {
		articles {
			data {
				attributes {
					title
				}
			}
		}
	}
`;

export const queryUserMe = gql`
	query UserMe {
		me {
			id
			username
			email
			confirmed
			blocked
			role {
				name
			}
		}
	}
`;

export const queryUsersPermissionsUser = gql`
	query userDetails($id: ID!) {
		usersPermissionsUser(id: $id) {
			data {
				id
				attributes {
					firstName
					lastName
					username
					email
					confirmed
					blocked
				}
			}
		}
	}
`;

export const queryArticlesBySlug = gql`
	${IMAGE_FIELDS}
	${META_FIELDS}
	${AUTHOR_FIELDS}
	query Articles($slug: String!) {
		articles(filters: { slug: { eq: $slug } }) {
			meta {
				...metaFields
			}
			data {
				attributes {
					title
					slug
					description
					content
					publishedAt
					createdAt
					image {
						...imageFields
					}
					category {
						data {
							attributes {
								name
								slug
							}
						}
					}
					author {
						...authorFields
					}
				}
			}
		}
	}
`;

export const pagesQuery = gql`
	query Pages {
		pages {
			data {
				attributes {
					title
				}
			}
		}
	}
`;

export const favouriteArticles = gql`
	query FavouriteArticles {
		usersPermissionsUser(id: 1) {
			data {
				attributes {
					favourite_articles {
						data {
							attributes {
								title
							}
						}
					}
				}
			}
		}
	}
`;

export default articles;
