import { ApolloClient, InMemoryCache, createHttpLink } from "@apollo/client";
import { setContext } from "@apollo/client/link/context";
import fetch from "isomorphic-unfetch";
import Cookies from "js-cookie";

const token = Cookies.get("token");
const GRAPHQL_URL = `${
	process.env.NEXT_PUBLIC_API_URL || "http://localhost:1337"
}/graphql`;

const httpLink = createHttpLink({
	uri: GRAPHQL_URL,
	fetch,
});

const authLink = setContext((_, { headers }) => {
	// Retrieve the authorization token from local storage.
	const token = Cookies.get("token");

	return {
		headers: {
			...headers,
			authorization: token ? `Bearer ${token}` : "",
		},
	};
});

const client = new ApolloClient({
	link: authLink.concat(httpLink),
	cache: new InMemoryCache(),
});

export default client;
