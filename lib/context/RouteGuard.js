import { useEffect, useState, useContext } from "react";
import { useRouter } from "next/router";
import AppContext from "/lib/context/AppContext";
import PageLoader from "/components/PageLoader/PageLoader";
import Cookies from "js-cookie";

// const routes = {
// 	private: [
// 		"/secure",
// 		"/secure/informationen",
// 		"/user",
// 		"/user/profile",
// 		"/user/account",
// 	],
// 	onlyPublic: ["/user/login"],
// };

const routes = {
	auth: ["/protected", "/user"],
	passCode: ["/registration", "/login"],
	publicOnly: ["/passcode"],
};

function RouteGuard({ children }) {
	const router = useRouter();
	const passCode = Cookies.get("passcode");
	const [authorized, setAuthorized] = useState(false);
	const { isAuthenticated, setAuthenticated, user } = useContext(AppContext);
	const isLoading = user?.loading;

	// const pathNeedsAuth = protectedRoutes.indexOf(router.asPath) !== -1;
	// console.log(router.asPath.indexOf(protectedRoutes));
	let pathNeedsAuth = false;
	routes.auth.map((route) => {
		router.asPath.length > 1 &&
			router.asPath.indexOf(route) !== -1 &&
			(pathNeedsAuth = true);
	});

	let pathNeedsPassCode = false;
	routes.passCode.map((route) => {
		router.asPath.length > 1 &&
			router.asPath.indexOf(route) !== -1 &&
			(pathNeedsPassCode = true);
	});

	let pathIsPublicOnly = false;
	routes.publicOnly.map((route) => {
		router.asPath.length > 1 &&
			router.route === route &&
			(pathIsPublicOnly = true);
	});

	useEffect(() => {
		if (!pathNeedsAuth && !pathIsPublicOnly && !pathNeedsPassCode) {
			setAuthorized(true);
		}
	}, [pathNeedsAuth, pathIsPublicOnly, pathNeedsPassCode]);

	// Public only
	useEffect(() => {
		if (!isLoading && pathIsPublicOnly) {
			if (passCode && !isAuthenticated) {
				router.push({
					pathname: "/login",
				});
				// setAuthorized(true);
			} else if (!passCode && !isAuthenticated) {
				setAuthorized(true);
			} else if (passCode && isAuthenticated) {
				router.push({
					pathname: "/user",
				});
				// setAuthorized(true);
			}
		}
	}, [isLoading, pathIsPublicOnly, isAuthenticated, passCode, router]);

	// Auth
	useEffect(() => {
		if (!isLoading && pathNeedsAuth && !isAuthenticated && passCode) {
			router.push({
				pathname: "/login",
				query: { redirect: router.asPath },
			});
		} else if (!isLoading && pathNeedsAuth && isAuthenticated) {
			setAuthorized(true);
		} else if (!isLoading && pathNeedsAuth && !isAuthenticated && !passCode) {
			router.push({
				pathname: "/passcode",
				query: { redirect: router.asPath },
			});
		}
	}, [isLoading, isAuthenticated, router, pathNeedsAuth, passCode]);

	// Passcode
	useEffect(() => {
		if (!isLoading && pathNeedsPassCode && !passCode) {
			router.push({
				pathname: "/passcode",
				query: { redirect: router.asPath },
			});
		} else if (
			!isLoading &&
			pathNeedsPassCode &&
			passCode &&
			!isAuthenticated
		) {
			setAuthorized(true);
		} else if (!isLoading && pathNeedsPassCode && passCode && isAuthenticated) {
			router.push({
				pathname: "/user",
				query: { redirect: router.asPath },
			});
		}
	}, [isLoading, pathNeedsPassCode, passCode, router, isAuthenticated]);

	if ((isLoading || !passCode) && pathNeedsPassCode) {
		return <PageLoader />;
	}

	if ((isLoading || !isAuthenticated) && pathNeedsAuth) {
		return <PageLoader />;
	}

	return authorized && children;

	// return children;
}

export default RouteGuard;
