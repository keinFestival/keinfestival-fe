import { gql } from '@apollo/client';

export const IMAGE_FIELDS = gql`
  fragment imageFields on UploadFileEntityResponse {
    data {
      attributes {
        name
        alternativeText
        caption
        width
        height
        formats
        hash
        ext
        mime
        size
        url
        previewUrl
        provider
        createdAt
        updatedAt
      }
    }
  }
`;

export const META_FIELDS = gql`
  fragment metaFields on ResponseCollectionMeta {
    pagination {
      total
      page
      pageSize
      pageCount
    }
  }
`;

export const AUTHOR_FIELDS = gql`
  ${IMAGE_FIELDS}
  fragment authorFields on UsersPermissionsUserEntityResponse {
    data {
      id
      attributes {
        username
        email
        firstName
        lastName
        picture {
          ...imageFields
        }
      }
    }
  }
`;