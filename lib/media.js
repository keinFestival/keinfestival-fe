/**
 * This function returns the correct URL of an image depending on where it is hosted (either on your local machine or hosted on a server).
 * Locally, an image has a URL structure like so: /uploads/… Whereas on Cloudinary it has a URL structure like this: https://cloudinary.com/....
 */

import { getStrapiURL } from "./api";

export function getStrapiMedia(media) {
	let imageUrl = null;

	if (media) {
		const { url } = media.data?.attributes || media;

		imageUrl = url?.startsWith("/") ? getStrapiURL(url) : url;
	} else {
		console.warning("no media defined");
	}

	return imageUrl;
}
