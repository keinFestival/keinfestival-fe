export const staticAppContext = {
    "user": {
        "id": 1,
        "username": "Robsa",
        "email": "rowphant@gmail.com",
        "provider": "local",
        "confirmed": true,
        "blocked": false,
        "createdAt": "2022-03-21T00:11:32.228Z",
        "updatedAt": "2022-04-05T17:20:46.015Z",
        "test": "blabla"
    },
    "isAuthenticated": true,
    "global": {
        "siteName": "JAM Stack",
        "createdAt": "2022-03-18T13:26:33.559Z",
        "updatedAt": "2022-03-29T17:08:28.349Z",
        "favicon": {
            "data": {
                "id": 10,
                "attributes": {
                    "name": "favicon",
                    "alternativeText": "favicon",
                    "caption": "favicon",
                    "width": 512,
                    "height": 512,
                    "formats": {
                        "thumbnail": {
                            "name": "thumbnail_favicon",
                            "hash": "thumbnail_favicon_d07b23493b",
                            "ext": ".png",
                            "mime": "image/png",
                            "path": null,
                            "width": 156,
                            "height": 156,
                            "size": 5.61,
                            "url": "/uploads/thumbnail_favicon_d07b23493b.png"
                        },
                        "small": {
                            "name": "small_favicon",
                            "hash": "small_favicon_d07b23493b",
                            "ext": ".png",
                            "mime": "image/png",
                            "path": null,
                            "width": 500,
                            "height": 500,
                            "size": 26.5,
                            "url": "/uploads/small_favicon_d07b23493b.png"
                        }
                    },
                    "hash": "favicon_d07b23493b",
                    "ext": ".png",
                    "mime": "image/png",
                    "size": 2.78,
                    "url": "/uploads/favicon_d07b23493b.png",
                    "previewUrl": null,
                    "provider": "local",
                    "provider_metadata": null,
                    "createdAt": "2022-03-18T13:26:33.408Z",
                    "updatedAt": "2022-03-18T13:26:33.408Z"
                }
            }
        },
        "defaultSeo": {
            "id": 2,
            "metaTitle": "JAM Stack",
            "metaDescription": "A blog made with Strapi, Next.js, Material UI and Storybook",
            "shareImage": {
                "data": {
                    "id": 11,
                    "attributes": {
                        "name": "default-image",
                        "alternativeText": "default-image",
                        "caption": "default-image",
                        "width": 1208,
                        "height": 715,
                        "formats": {
                            "thumbnail": {
                                "name": "thumbnail_default-image",
                                "hash": "thumbnail_default_image_77d8973bca",
                                "ext": ".png",
                                "mime": "image/png",
                                "path": null,
                                "width": 245,
                                "height": 145,
                                "size": 18.69,
                                "url": "/uploads/thumbnail_default_image_77d8973bca.png"
                            },
                            "large": {
                                "name": "large_default-image",
                                "hash": "large_default_image_77d8973bca",
                                "ext": ".png",
                                "mime": "image/png",
                                "path": null,
                                "width": 1000,
                                "height": 592,
                                "size": 297.46,
                                "url": "/uploads/large_default_image_77d8973bca.png"
                            },
                            "medium": {
                                "name": "medium_default-image",
                                "hash": "medium_default_image_77d8973bca",
                                "ext": ".png",
                                "mime": "image/png",
                                "path": null,
                                "width": 750,
                                "height": 444,
                                "size": 159.3,
                                "url": "/uploads/medium_default_image_77d8973bca.png"
                            },
                            "small": {
                                "name": "small_default-image",
                                "hash": "small_default_image_77d8973bca",
                                "ext": ".png",
                                "mime": "image/png",
                                "path": null,
                                "width": 500,
                                "height": 296,
                                "size": 63.44,
                                "url": "/uploads/small_default_image_77d8973bca.png"
                            }
                        },
                        "hash": "default_image_77d8973bca",
                        "ext": ".png",
                        "mime": "image/png",
                        "size": 81.73,
                        "url": "/uploads/default_image_77d8973bca.png",
                        "previewUrl": null,
                        "provider": "local",
                        "provider_metadata": null,
                        "createdAt": "2022-03-18T13:26:33.556Z",
                        "updatedAt": "2022-03-18T13:26:33.556Z"
                    }
                }
            }
        },
        "nav": [
            {
                "title": "Home",
                "slug": "home",
                "path": "/page"
            },
            {
                "title": "Articles",
                "slug": "articles",
                "path": "/page"
            }
        ]
    },
    "showLogin": false
}