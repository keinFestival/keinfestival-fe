export const staticUserQuery = {
  "data": {
    "usersPermissionsUser": {
        "__typename": "UsersPermissionsUserEntityResponse",
        "data": {
            "__typename": "UsersPermissionsUserEntity",
            "id": "1",
            "attributes": {
                "__typename": "UsersPermissionsUser",
                "username": "Rob",
                "email": "rowphant@gmail.com",
                "confirmed": true,
                "blocked": false,
                "favourite_articles": {
                    "__typename": "ArticleRelationResponseCollection",
                    "data": [
                        {
                            "__typename": "ArticleEntity",
                            "id": "2",
                            "attributes": {
                                "__typename": "Article",
                                "title": "This shrimp is awesome"
                            }
                        },
                        {
                            "__typename": "ArticleEntity",
                            "id": "5",
                            "attributes": {
                                "__typename": "Article",
                                "title": "Beautiful picture"
                            }
                        }
                    ]
                }
            }
        }
    }
  }
}