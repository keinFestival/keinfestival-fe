/**
 * fetStrapiURL:
 * By default, this function will look at a NEXT_PUBLIC_STRAPI_API_URL env variable in your .env file.
 * If you don't have one, it will use the default url of Strapi on your machine.
 *
 * fetchAPI:
 * This function gets the request URL thanks to the getStrapiURL function above.
 * Then it calls the fetch function on this requestUrl with some parameters that are stringifyed and returns the data in a JSON format.
 */

import qs from "qs";

/**
 * Get full Strapi URL from path
 * @param {string} path Path of the URL
 * @returns {string} Full Strapi URL
 */
export function getStrapiURL(path = "") {
	return `${process.env.NEXT_PUBLIC_API_URL || "http://localhost:1337"}${path}`;
}

/**
 * Helper to make GET requests to Strapi API endpoints
 * @param {string} path Path of the API route
 * @param {Object} urlParamsObject URL params object, will be stringified
 * @param {Object} options Options passed to fetch
 * @returns Parsed API call response
 */
export async function fetchAPI(path, urlParamsObject = {}, options = {}) {
	// Merge default and user options
	const mergedOptions = {
		headers: {
			"Content-Type": "application/json",
		},
		...options,
	};

	// Build request URL
	const queryString = qs.stringify(urlParamsObject);
	const requestUrl = `${getStrapiURL(
		`/api${path}${queryString ? `?${queryString}` : ""}`
	)}`;

	// Trigger API call
	const response = await fetch(requestUrl, mergedOptions);

	// Handle response
	if (
		!response.ok &&
		options.method !== "post" &&
		options.method !== "PUT" &&
		path !== "/users/me" &&
		path !== "/users"
	) {
		console.error(response.statusText);
		throw new Error(`An error occured please try again`);
	}
	const data = await response.json();
	return data;
}

//   // Trigger API call
//   const response = await fetch(requestUrl, mergedOptions);

//   // console.log('response: ', response)

//   // Handle response
//   if (!response.ok && options.method !== 'post' && options.method !== 'PUT') {
//     console.error(response.statusText);
//     throw new Error(`An error occured please try again`);
//   }
//   const data = await response.json();
//   return data;
// }
