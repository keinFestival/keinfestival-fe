import { createTheme, responsiveFontSizes } from "@mui/material/styles";
import { deepPurple, amber } from "@mui/material/colors";

const violet = {
	light: "#654173",
	main: "#391847",
	dark: "#180020",
};

const blue = {
	light: "#66ffff",
	main: "#00d0dd",
	dark: "#009eab",
};

// Theme core
const coreThemeObj = {
	typography: {
		fontFamily: ['"Barlow", Helvetica, Arial, sans-serif'].join(","),
		fontSize: 18,
		fontWeightLight: 200,
		fontWeightMedium: 600,
		fontWeightRegular: 400,
		button: {
			fontWeight: 500,
		},
		h3: {
			fontWeight: 200,
		},
	},
	components: {
		MuiCssBaseline: {
			styleOverrides: `
				html {
					letter-spacing: 1pt;
				}
        /* barlow-200 - latin */
        @font-face {
          font-family: 'Barlow';
          font-style: normal;
          font-weight: 200;
          src: local(''),
              url('../fonts/barlow-v12-latin-200.woff2') format('woff2'), /* Chrome 26+, Opera 23+, Firefox 39+ */
              url('../fonts/barlow-v12-latin-200.woff') format('woff'); /* Chrome 6+, Firefox 3.6+, IE 9+, Safari 5.1+ */
        }
        /* barlow-regular - latin */
        @font-face {
          font-family: 'Barlow';
          font-style: normal;
          font-weight: 400;
          src: local(''),
              url('../fonts/barlow-v12-latin-regular.woff2') format('woff2'), /* Chrome 26+, Opera 23+, Firefox 39+ */
              url('../fonts/barlow-v12-latin-regular.woff') format('woff'); /* Chrome 6+, Firefox 3.6+, IE 9+, Safari 5.1+ */
        }
        /* barlow-600 - latin */
        @font-face {
          font-family: 'Barlow';
          font-style: normal;
          font-weight: 600;
          src: local(''),
              url('../fonts/barlow-v12-latin-600.woff2') format('woff2'), /* Chrome 26+, Opera 23+, Firefox 39+ */
              url('../fonts/barlow-v12-latin-600.woff') format('woff'); /* Chrome 6+, Firefox 3.6+, IE 9+, Safari 5.1+ */
        }
      `,
		},
		MuiButton: {
			styleOverrides: {
				root: {
					borderRadius: 0,
					fontWeight: 400,
					letterSpacing: "1pt",
				},
			},
		},
		// MuiAppBar: {
		// 	styleOverrides: {
		// 		root: {
		// 			backgroundColor: violet.dark,
		// 		},
		// 	},
		// },
	},
};

// Dark theme
const createDarkTheme = createTheme({
	...coreThemeObj,
	palette: {
		mode: "dark",
		primary: {
			main: blue.main,
		},
		secondary: {
			main: violet.main,
		},
		background: {
			default: violet.dark,
			paper: violet.dark,
		},
		// divider: "rgba(255,255,255,0.12)",
		// text: {
		// 	primary: "#ffffff",
		// },
	},
	components: {
		...coreThemeObj.components,
		// MuiAppBar: {
		// 	styleOverrides: {
		// 		root: {
		// 			backgroundColor: violet.dark,
		// 		},
		// 	},
		// },
	},
});
export const darkTheme = responsiveFontSizes(createDarkTheme);

// Light theme
const createLightTheme = createTheme({
	...coreThemeObj,
	palette: {
		mode: "light",
		primary: {
			main: blue.dark,
		},
		secondary: {
			main: violet.main,
		},
	},
	components: {
		...coreThemeObj.components,
		MuiAppBar: {
			styleOverrides: {
				root: {
					backgroundColor: violet.main,
				},
			},
		},
	},
});
export const lightTheme = responsiveFontSizes(createLightTheme);

// Default theme
let defaultTheme = createTheme({
	palette: {
		// primary: deepPurple,
		// secondary: amber,
	},
});

defaultTheme = responsiveFontSizes(defaultTheme);

export default defaultTheme;
