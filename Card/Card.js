import React from "react";
import { motion } from "framer-motion";
import Link from "next/link";
import Image from "/components/Image/Image";

import {
	Card as MuiCard,
	CardContent,
	CardMedia,
	CardActionArea,
	CardActions,
	Typography,
	Button,
	Zoom,
	Grow,
	Fade,
} from "@mui/material";

const variants = {
	hidden: { opacity: 0, x: 0, y: 0, scale: 0.5 },
	enter: { opacity: 1, x: 0, y: 0, scale: 1 },
	// exit: { opacity: 0, x: 0, y: 0, scale: 2 },
};

const Card = (props) => {
	const { article, index = null } = props;
	let card = article.attributes;

	if (card.image.data) {
		card = {
			...card,
			image: {
				data: {
					attributes: {
						objectFit: card.objectFit,
						ratio: card.ratio
					}
				}
			}
		}
	}

	return (
		card && (
			// <Fade
			// 	in={true}
			// 	style={{
			// 		transitionDelay: index ? `${index * 300}ms` : "0ms",
			// 		transitionDuration: "500ms",
			// 	}}
			// >
			<motion.div
				// initial="hidden"
				// animate="enter"
				// exit="exit"
				whileTap={{ scale: 1.07 }}
				whileFocus={{ scale: 1.07 }}
				whileHover={{ scale: 1.05 }}
				// variants={variants}
				transition={{ duration: 0.2 }}
			>
				<MuiCard>
					<Link href={`/article/${card.slug}`} passHref scroll={false}>
						<CardActionArea>
							{card.image?.data && (
								<Image image={card.image.data?.attributes} alt="" />
							)}
							<CardContent>
								<Typography gutterBottom variant="h5" component="div">
									{card.title}
								</Typography>
								<Typography variant="body2" color="text.secondary">
									{card.description}
								</Typography>
							</CardContent>
						</CardActionArea>
					</Link>
					{card.category.data?.attributes.slug && (
						<CardActions>
							<Link
								href={`/category/${card.category.data.attributes.slug}`}
								passHref
							>
								<Button size="small" color="primary">
									{card.category.data.attributes.name}
								</Button>
							</Link>
						</CardActions>
					)}
				</MuiCard>
			</motion.div>
			// </Fade>
		)
	);
};

export default Card;
