import React from "react";
import Card from "./Card";

import { Box } from "@mui/material";

import * as ImageStories from "/components/Image/Image.stories";

const defaultExport = {
	title: "Components/Teaser/Card",
	component: Card,
	argTypes: {
		...ImageStories.default.argTypes,
	},
};

const Template = (args) => (
	<Box sx={{ maxWidth: 400 }}>
		<Card article={{ attributes: { ...args } }} />
	</Box>
);

export const Default = Template.bind({});
Default.args = {
	slug: "the-internet-s-own-boy",
	title: "The internet's Own boy",
	description:
		"Follow the story of Aaron Swartz, the boy who could change the world",
	category: {
		data: {
			attributes: {
				name: "story",
				slug: "story",
			},
		},
	},
	ratio: ImageStories.Default.args.ratio,
	objectFit: ImageStories.Default.args.objectFit,
	image: {
		data: {
			attributes: {
				...ImageStories.Default.args,
			},
		},
	},
};

export default defaultExport;
