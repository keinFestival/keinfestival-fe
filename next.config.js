module.exports = {
	reactStrictMode: true,
	// basePath: "/dist/out",
	trailingSlash: true,
	images: {
		// disableStaticImages: true,
		// loader: "default",
		loader: "custom",
		domains: ["localhost", "via.placeholder.com"],
		deviceSizes: [640, 750, 828, 1080, 1200, 1920, 2048, 3840],
	},
	exportPathMap: async function (
		defaultPathMap,
		{ dev, dir, outDir, distDir, buildId }
	) {
		return {
			// "/": { page: "/" },
			// "/_documents": { page: "/" },
		};
	},
	// This is telling webpack how to load the .graphql files
	webpack: (config) => {
		config.module.rules.push({
			test: /\.(graphql|gql)$/,
			exclude: /node_modules/,
			loader: "graphql-tag/loader",
		});
		return config;
	},
	webpackDevMiddleware: (config) => {
		return config;
	},
};
