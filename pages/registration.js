import React, { useState, useContext, useEffect, useCallback } from "react";
import NextLink from "next/link";
import { AnimatePresence, motion } from "framer-motion";

import { registerUser } from "/lib/auth";
import AppContext from "/lib/context/AppContext";

import {
	Container,
	Stack,
	Box,
	TextField,
	Paper,
	Typography,
	Button,
	Link,
} from "@mui/material";

import Layout from "/components/layout";

const inputVariant = "outlined";

const variants = {
	hidden: { opacity: 0, x: 0, y: 10 },
	enter: {
		opacity: 1,
		x: 0,
		y: 0,
		scale: 1,
	},
	exit: { opacity: 0, x: 0, y: 10 },
};

const Register = ({ global, props }) => {
	const { setUser } = useContext(AppContext);
	const [data, setData] = useState({
		firstName: "",
		lastName: "",
		email: "",
		// username: "",
		password: "",
	});
	const [loading, setLoading] = useState(false);
	const [error, setError] = useState({});
	const [validation, setValidation] = useState({});
	const [isSubmitted, setSubmitted] = useState(false);

	useEffect(() => {
		const listener = (event) => {
			if (event.code === "Enter" || event.code === "NumpadEnter") {
				event.preventDefault();
				submitHandler(data);
			}
		};
		document.addEventListener("keydown", listener);
		return () => {
			document.removeEventListener("keydown", listener);
		};
	}, [data, submitHandler]);

	const submitHandler = useCallback(
		(data) => {
			setLoading(true);
			registerUser(data.firstName, data.lastName, data.email, data.password)
				.then((res) => {
					// set authed user in global context object
					setUser(res.data.user);
					setSubmitted(true);
					setLoading(false);
				})
				.catch((error) => {
					setError(error.response.data);
					setLoading(false);
				});
		},
		[data]
	);

	useEffect(() => {
		console.log("new error");
		let temp = {};

		error?.error?.details?.errors?.forEach((error, index) => {
			console.log(index, error);
			console.log(error.path[0]);

			temp = {
				...temp,
				[error.path[0]]: { ...error },
			};
		});

		setValidation({
			...temp,
		});
	}, [loading, error?.error?.details?.errors]);

	return (
		<Layout nav={global.attributes.nav} type="blank">
			<Container
				maxWidth="sm"
				sx={{
					py: 10,
				}}
			>
				<AnimatePresence exitBeforeEnter>
					<motion.div
						animate={{ opacity: 1, scale: 1 }}
						initial={{ opacity: 0, scale: 0.9 }}
						exit={{ opacity: 0, scale: 0.9 }}
						transition={{ duration: 0.25 }}
						key={isSubmitted}
					>
						{!isSubmitted ? (
							<form>
								<Box
									component="fieldset"
									sx={{
										display: "flex",
										flexWrap: "wrap",
										flexDirection: "column",
										border: "none",
										p: 0,
									}}
									noValidate
									autoComplete="off"
									disabled={loading}
								>
									<Typography variant="p" component="div" sx={{ mb: 0 }}>
										Registrierung
									</Typography>
									<Typography
										variant="h4"
										component="div"
										sx={{ mb: 6, letterSpacing: "2pt" }}
									>
										<span style={{ fontWeight: 600 }}>kein</span>
										<span style={{ fontWeight: 200 }}>Festival</span>
									</Typography>

									<Stack spacing={2} direction="row" sx={{ mb: 2 }}>
										<TextField
											error={Boolean(validation.firstName)}
											helperText={validation.firstName?.message}
											label="First Name"
											value={data.firstName}
											type="text"
											name="firstName"
											variant={inputVariant}
											disabled={loading}
											onChange={(e) =>
												setData({ ...data, firstName: e.target.value })
											}
										/>
										<TextField
											error={Boolean(validation.lastName)}
											helperText={validation.lastName?.message}
											label="Last Name"
											value={data.lastName}
											type="text"
											name="lastName"
											variant={inputVariant}
											disabled={loading}
											onChange={(e) =>
												setData({ ...data, lastName: e.target.value })
											}
										/>
									</Stack>
									<Stack spacing={2}>
										<TextField
											error={Boolean(validation.email)}
											helperText={validation.email?.message}
											label="E-Mail"
											value={data.email}
											type="email"
											name="email"
											variant={inputVariant}
											onChange={(e) =>
												setData({ ...data, email: e.target.value })
											}
										/>
										<TextField
											error={Boolean(validation.password)}
											helperText={validation.password?.message}
											label="Password"
											value={data.password}
											type="password"
											name="password"
											variant={inputVariant}
											onChange={(e) =>
												setData({ ...data, password: e.target.value })
											}
										/>
									</Stack>

									<Stack
										spacing={2}
										sx={{ pt: 2 }}
										direction="row"
										justifyContent={"space-between"}
									>
										{/* <NextLink href={`/account/reset-password`} passHref>
										<Link color="gray" underline="hover">
											<Typography variant="caption" noWrap component="div">
												Passwort vergessen?
											</Typography>
										</Link>
									</NextLink> */}
										<NextLink href={`/login`} passHref>
											<Link color="gray" underline="hover">
												<Typography variant="caption" noWrap component="div">
													Ich bin schon registriert
												</Typography>
											</Link>
										</NextLink>
										{/* <Typography
										variant="caption"
										noWrap
										component="div"
										color="gray"
									>
										Was passiert mit meinen Daten?
									</Typography> */}
									</Stack>
									<Box sx={{ pt: 4, width: "100%" }}>
										{error?.error && (
											<div style={{ marginBottom: 10 }}>
												<small style={{ color: "red" }}>
													{error.error.message}
												</small>
											</div>
										)}
										<Button
											variant="contained"
											size="large"
											disabled={loading}
											fullWidth
											onClick={() => submitHandler()}
										>
											{loading ? "Loading.." : "Submit"}
										</Button>
									</Box>
								</Box>
							</form>
						) : (
							<Paper elevation={2} sx={{ p: 6 }}>
								<Stack spacing={4}>
									<Typography variant="h3">
										<small>Fast geschafft!</small>
									</Typography>
									<Stack spacing={2}>
										<Typography variant="h5">
											Wir senden dir in den nächsten 5 Minuten eine E-Mail.
											<br />
											Klicke auf den darin enthaltenen Link, um deine
											Registrierung abzuschließen.
										</Typography>
										<Typography variant="caption">
											Vergiss nicht, auch in deinem Spam-Ordner nach der
											Bestätigungs-Mail zu sehen, falls nach 5 Minuten, noch
											immer keine E-Mail in deinem Posteingang angekommen ist.
										</Typography>
									</Stack>
								</Stack>
							</Paper>
						)}
					</motion.div>
				</AnimatePresence>
			</Container>
		</Layout>
	);
};

export async function getStaticProps(ctx) {
	return {
		props: {},
	};
}

export default Register;
