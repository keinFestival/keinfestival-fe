import NextLink from "next/link"
import {
  Container,
  Grid,
  Typography,
  Link,
  Button
  } from "@mui/material"
import Layout from "/components/layout";

export default function Error404({global}) {
  return (
    <Layout nav={global.attributes.nav}>
      <Container maxWidth="sm" sx={{py: 18}}>
        <Grid container justifyContent="start">
          <Grid item xs={12}>
            <Typography variant="h1">Error <b>404</b></Typography>
            <hr/>
            <Typography variant="h4" textAlign="right" sx={{pt: 1, px: 1}}>Page Not Found</Typography>
          </Grid>
          <Grid item xs={12} sx={{mt: 8}} textAlign="center">
            <NextLink href={`/`} passHref>
              <Button variant="outlined" fullWidth>
                Go to start page
              </Button>
          </NextLink>
          </Grid>
        </Grid>
      </Container>
    </Layout>
  )
}