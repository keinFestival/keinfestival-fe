import React, { useState, useContext } from "react";
import NextLink from "next/link";

import { registerUser } from "/lib/auth";
import AppContext from "/lib/context/AppContext";

import {
	Container,
	Box,
	TextField,
	Paper,
	Typography,
	Button,
	Link,
} from "@mui/material";

import Layout from "/components/layout";

const Register = ({ global }) => {
	const [data, setData] = useState({ email: "", username: "", password: "" });
	const [loading, setLoading] = useState(false);
	const [error, setError] = useState({});

	const { setUser } = useContext(AppContext);

	return (
		<Layout nav={global.attributes.nav}>
			<Container
				maxWidth="xs"
				sx={{
					py: 10,
				}}
			>
				<Paper elevation={3}>
					<form>
						<Box
							component="fieldset"
							sx={{
								px: 2,
								py: 4,
								display: "flex",
								flexWrap: "wrap",
								flexDirection: "column",
								border: "none",
								"& > :not(:last-child):not(style)": { mb: 3 },
							}}
							noValidate
							autoComplete="off"
							disabled={loading}
						>
							<Typography variant="h4" component="div">
								Register
							</Typography>
							{Object.entries(error).length !== 0 &&
								error.constructor === Object &&
								error.message.map((error) => {
									return (
										<div
											key={error.messages[0].id}
											style={{ marginBottom: 10 }}
										>
											<small style={{ color: "red" }}>
												{error.messages[0].message}
											</small>
										</div>
									);
								})}
							<TextField
								label="Username"
								value={data.username}
								type="text"
								name="username"
								variant="outlined"
								style={{ height: 50, fontSize: "1.2em" }}
								disabled={loading}
								onChange={(e) => setData({ ...data, username: e.target.value })}
							/>
							<TextField
								label="E-Mail"
								value={data.email}
								type="email"
								name="email"
								variant="outlined"
								onChange={(e) => setData({ ...data, email: e.target.value })}
							/>
							<TextField
								label="Password"
								value={data.password}
								type="password"
								name="password"
								variant="outlined"
								onChange={(e) => setData({ ...data, password: e.target.value })}
							/>

							<NextLink href={`/account/reset-password`} passHref>
								<Link color="gray" underline="hover">
									<Typography variant="caption" noWrap component="div">
										Forgot password?
									</Typography>
								</Link>
							</NextLink>

							<Button
								variant="contained"
								size="large"
								sx={{ mt: 1 }}
								disabled={loading}
								onClick={() => {
									setLoading(true);
									registerUser(data.username, data.email, data.password)
										.then((res) => {
											// set authed user in global context object
											setUser(res.data.user);
											setLoading(false);
										})
										.catch((error) => {
											setError(error.response.data);
											setLoading(false);
										});
								}}
							>
								{loading ? "Loading.." : "Submit"}
							</Button>
						</Box>
					</form>
				</Paper>
			</Container>
		</Layout>
	);
};
export default Register;
