import React, { useEffect, useState, useContext } from "react";
import { useQuery } from "@apollo/client";
// import { queryUsersPermissionsUser } from "/lib/queries.js";
import Cookies from "js-cookie";
import { fetchAPI } from "/lib/api";
import AppContext from "/lib/context/AppContext";
import { Container, Typography, Stack, Paper, Grid } from "@mui/material";

import Layout from "/components/layout";
import Account from "/components/Account/Account";
import Ticketing from "/components/Ticketing/Ticketing";

export default function AccountPage({ global }) {
	const { user } = useContext(AppContext);

	return (
		<Layout nav={global.attributes.nav}>
			<Container maxWidth="md">
				<Stack spacing={4} sx={{ pt: 10 }}>
					<Typography>Hallo {user.data.firstName}!</Typography>
					<Paper elevation={0} sx={{ p: 0 }}>
						<Grid container spacing={2}>
							<Grid item xs={12} sm={6}>
								<Paper elevation={1} sx={{ p: 4 }}>
									<Typography variant="h5" sx={{ mb: 3 }}>
										Ticket 🎫
									</Typography>
									<Ticketing />
								</Paper>
							</Grid>
							<Grid item xs={12} sm={6}>
								<Paper elevation={1} sx={{ p: 4 }}>
									<Typography variant="h5" sx={{ mb: 3 }}>
										Supporter shifts 💪
									</Typography>
									<Typography variant="subtitle1">
										🚧 Work in progress
									</Typography>
									{/* <Ticketing /> */}
								</Paper>
							</Grid>
							<Grid item xs={12}>
								<Paper elevation={1} sx={{ p: 4 }}>
									<Typography variant="h6" sx={{ mb: 2 }}>
										Weitere Informationen
									</Typography>
									<Typography variant="p">...</Typography>
									{/* <Ticketing /> */}
								</Paper>
							</Grid>
						</Grid>
					</Paper>
				</Stack>
				{/* <Account user={appContext.user} /> */}
			</Container>
		</Layout>
	);
}

export async function getStaticProps(ctx) {
	return {
		props: {},
	};
}
