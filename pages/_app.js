import React, { createContext, useState, useEffect } from "react";
import { useRouter } from "next/router";
import Head from "next/head";
import Cookies from "js-cookie";
import { ApolloProvider } from "@apollo/client";

import client from "/lib/apollo";
import { fetchAPI } from "/lib/api";
import { getStrapiMedia } from "/lib/media";

import RouteGuard from "/lib/context/RouteGuard";
import AppContext from "/lib/context/AppContext";

import createEmotionCache from "/styles/createEmotionCache";
import { darkTheme, lightTheme } from "/styles/theme";
import ColorModeContext from "/styles/ColorModeContext";

import { ThemeProvider } from "@mui/material/styles";
import CssBaseline from "@mui/material/CssBaseline";
import { useMediaQuery } from "@mui/material";

import { AnimatePresence } from "framer-motion";

// Client-side cache, shared for the whole session of the user in the browser.
const clientSideEmotionCache = createEmotionCache();

// grab token value from cookie
const token = Cookies.get("token");

const MyApp = (props) => {
	const { Component, emotionCache = clientSideEmotionCache, pageProps } = props;
	const { global, userProps } = pageProps;
	const favicon = global.attributes.favicon;

	const router = useRouter();

	// Set dark mode based on media query
	const prefersDarkMode = useMediaQuery("(prefers-color-scheme: dark)");
	// const [darkMode, setDarkMode] = useState(prefersDarkMode);
	const [darkMode, setDarkMode] = useState(true);
	const [user, setUser] = useState({ loading: true });
	const [isAuthenticated, setAuthenticated] = useState(false);
	const [showLogin, setShowLogin] = useState(false);

	// useEffect(() => {
	// 	const mode = localStorage.getItem("mode") === "true";
	// 	setDarkMode(mode);
	// }, []);

	const _setDarkMode = (newmode) => {
		localStorage.setItem("mode", newmode);
		setDarkMode(newmode);
	};

	useEffect(() => {
		setUser({ loading: true });
		fetchAPI(
			"/users/me",
			{ populate: "*" },
			{
				headers: {
					Authorization: token ? `Bearer ${token}` : "",
					"Content-Type": "application/json",
				},
			}
		).then((data) => {
			if (data.id) {
				fetchAPI(
					"/users",
					{
						fields: { ticket_category: "*" },
						filters: {
							id: data.id,
						},
					},
					{
						headers: {
							Authorization: token ? `Bearer ${token}` : "",
							"Content-Type": "application/json",
						},
						method: "get",
					}
				).then((data) => {
					setAuthenticated(true);
					setUser({ data: data[0], loading: false });
				});
			} else {
				setAuthenticated(false);
				setUser({ error: true, loading: false });
			}
		});
	}, []);

	return (
		<ApolloProvider client={client}>
			<AppContext.Provider
				value={{
					user: user,
					isAuthenticated: isAuthenticated,
					setAuthenticated: setAuthenticated,
					setUser: setUser,
					global: global.attributes,
					siteName: global.attributes?.siteName,
					showLogin: showLogin,
					setShowLogin: setShowLogin,
				}}
			>
				<Head>
					<title>{global.attributes?.siteName}</title>
					<meta name="viewport" content="initial-scale=1, width=device-width" />
					<link rel="shortcut icon" href={getStrapiMedia(favicon)} />
				</Head>
				<ColorModeContext.Provider
					value={{ darkMode, setDarkMode: _setDarkMode }}
				>
					<ThemeProvider theme={darkMode ? darkTheme : lightTheme}>
						{/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
						<CssBaseline />
						<AnimatePresence
							exitBeforeEnter
							initial={true}
							onExitComplete={() => window.scrollTo(0, 0)}
						>
							<RouteGuard>
								<Component {...pageProps} key={router.asPath} />
							</RouteGuard>
						</AnimatePresence>
					</ThemeProvider>
				</ColorModeContext.Provider>
			</AppContext.Provider>
		</ApolloProvider>
	);
};

// Store Strapi Global object in context
export const GlobalContext = createContext({});

// getInitialProps disables automatic static optimization for pages that don't
// have getStaticProps. So article, category and home pages still get SSG.
// Hopefully we can replace this with getStaticProps once this issue is fixed:
// https://github.com/vercel/next.js/discussions/10949
MyApp.getInitialProps = async (props) => {
	// Fetch global site and pages settings from Strapi
	const [globalRes, pagesRes] = await Promise.all([
		fetchAPI("/global", {
			populate: {
				favicon: "*",
				defaultSeo: {
					populate: "*",
				},
			},
		}),
		fetchAPI("/pages", { populate: "*" }),
	]);

	const nav = [];

	pagesRes.data.map((page) => {
		nav.push({
			title: page.attributes.title,
			slug: page.attributes.slug,
			path: "/page",
		});
	});

	// Pass the data to our page via props
	return {
		pageProps: {
			global: {
				attributes: {
					...globalRes.data.attributes,
					nav: nav,
				},
			},
		},
	};
};

export default MyApp;
