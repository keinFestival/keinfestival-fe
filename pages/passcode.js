import React, { useState, useContext, useEffect } from "react";
import NextLink from "next/link";

import { registerUser } from "/lib/auth";
import AppContext from "/lib/context/AppContext";

import {
	Container,
	Stack,
	Box,
	TextField,
	Paper,
	Typography,
	Button,
	Link,
} from "@mui/material";

import Layout from "/components/layout";
import PassCodeComponent from "/components/PassCode/PassCode";

const inputVariant = "outlined";

const PassCode = ({ global, props }) => {
	return (
		<Layout nav={global.attributes.nav} type="blank">
			<Container
				maxWidth="sm"
				sx={{
					py: 10,
				}}
			>
				<PassCodeComponent />
			</Container>
		</Layout>
	);
};

export async function getStaticProps(ctx) {
	return {
		props: {},
	};
}

export default PassCode;
