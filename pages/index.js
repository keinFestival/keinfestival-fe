import React from "react";
import Container from "@mui/material/Container";
import Landingpage from "/components/pages/Landingpage/Landingpage";
import Layout from "../components/layout";
import Seo from "../components/seo";
import { fetchAPI } from "../lib/api";
import ApolloClient from "../lib/apollo";
// import { pagesQuery } from "/lib/queries.js";

const Home = ({ graphql, global, articles, homepage }) => {
	return (
		<Layout nav={global.attributes.nav} type="blank">
			<Landingpage />
		</Layout>
	);
};

export async function getStaticProps(ctx) {
	// Run API calls in parallel
	const [articlesRes, categoriesRes, homepageRes] = await Promise.all([
		fetchAPI("/articles", {
			populate: ["image", "category"],
			sort: ["createdAt:desc"],
		}),
		fetchAPI("/categories", { populate: "*" }),
		fetchAPI("/homepage", {
			populate: {
				hero: "*",
				seo: { populate: "*" },
			},
		}),
	]);

	// const {
	//   loading,
	//   error,
	//   data: {pages}
	// } = await ApolloClient.query({query: pagesQuery})

	return {
		props: {
			// graphql: data.articles.data,
			// graphql: pages.data,
			articles: articlesRes.data,
			categories: categoriesRes.data,
			homepage: homepageRes.data,
		},
		revalidate: 1,
	};
}

export default Home;
