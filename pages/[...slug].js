import React, { useContext, useEffect, useState } from "react";
import { useRouter } from "next/router";
import ReactMarkdown from "react-markdown";
import Container from "@mui/material/Container";

import { fetchAPI } from "/lib/api";
import AppContext from "/lib/context/AppContext";
import Layout from "/components/layout";

import { useQuery } from "@apollo/client";
// import { queryUserMe } from "/lib/queries.js";

const Page = ({ global, page }) => {
	const { setShowLogin, user } = useContext(AppContext);
	const { access, setAccess } = useState(false);
	const title = page?.attributes?.title;
	const isPublic = page.attributes.permissionRole !== "authenticated";
	const router = useRouter();
	const permissionRole = page.attributes.permissionRole;

	// useEffect(() => {
	// 	if ((user?.loading != false && user.data) || isPublic) {
	// 		setAccess(true);
	// 	}
	// }, [user]);

	// if (!user.loading && !access) {
	// router.push("/");
	// return router.push("/");
	// }

	return (
		// !user.loading &&
		// access && (
		<Layout
			nav={global?.attributes?.nav}
			type={permissionRole !== "authenticated" && "blank"}
		>
			<Container maxWidth="lg">
				<h1>{title}</h1>
				<ReactMarkdown>{page?.attributes?.content}</ReactMarkdown>
			</Container>
		</Layout>
		// )
	);
};

export async function getStaticPaths() {
	const pagesRes = await fetchAPI("/pages", {
		fields: ["slug", "permissionRole"],
	});

	return {
		paths: pagesRes.data?.map((page) => {
			const path =
				page.attributes.permissionRole === "authenticated" ? "/protected" : "";

			return `${path}/${page.attributes.slug}`;
		}),
		fallback: false,
	};
}

export async function getStaticProps({ params }) {
	const pagesRes = await fetchAPI("/pages", {
		populate: "*",
		filters: {
			slug: params.slug,
		},
	});

	return {
		props: {
			page: pagesRes?.data?.[0],
		},
		revalidate: 10,
	};
}

export default Page;
