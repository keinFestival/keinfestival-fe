import React, { useContext } from "react";
import { Container, Box, Paper, Typography, Stack } from "@mui/material";
import AppContext from "/lib/context/AppContext";
import Layout from "/components/layout";
import Login from "/components/Login/Login";

export default function LoginPage({ global }) {
	return (
		<Layout nav={global.attributes.nav} type="blank">
			<Container
				maxWidth="sm"
				sx={{
					py: 10,
				}}
			>
				<Stack spacing={0} sx={{ mb: 8 }}>
					<Typography variant="h4" component="h2" sx={{ mb: 4 }}>
						E-Mailadresse bestätigt&nbsp;&nbsp;&nbsp;✔
					</Typography>
					<Typography variant="p">
						Du hast es fast geschafft!
						{/* Deine Registrierung für
						<span style={{ fontWeight: 600 }}> kein</span>
						<span style={{ fontWeight: 200 }}>Festival </span>
						ist fast abgeschlossen! */}
					</Typography>
					<Typography variant="p">
						Logge dich jetzt ein, um die Registrierung abzuschließen.
					</Typography>
				</Stack>
				{/* <Paper elevation={1} sx={{ p: 4 }}> */}
				<Login />
				{/* </Paper> */}
			</Container>
		</Layout>
	);
}

export async function getStaticProps(ctx) {
	return {
		props: {},
	};
}
