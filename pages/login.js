import React, { useContext } from "react";
import { Container, Box, Paper, Typography, Stack } from "@mui/material";
import AppContext from "/lib/context/AppContext";
import Layout from "/components/layout";
import Login from "/components/Login/Login";

export default function LoginPage({ global }) {
	return (
		<Layout nav={global.attributes.nav} type="blank">
			<Container
				maxWidth="sm"
				sx={{
					py: 10,
				}}
			>
				<Stack spacing={0} sx={{ mb: 6 }}>
					<Typography variant="p" component="div" sx={{ mb: 0 }}>
						Login
					</Typography>
					<Typography
						variant="h4"
						component="div"
						sx={{ mb: 1, letterSpacing: "2pt" }}
					>
						<span style={{ fontWeight: 600 }}>kein</span>
						<span style={{ fontWeight: 200 }}>Festival</span>
					</Typography>
				</Stack>
				<Login />
			</Container>
		</Layout>
	);
}

export async function getStaticProps(ctx) {
	return {
		props: {},
	};
}
