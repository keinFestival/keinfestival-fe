import { useEffect, useState, useCallback } from "react";
import {
	Container,
	Paper,
	Typography,
	Button,
	Grid,
	Box,
	Stack,
	TextField,
	Snackbar,
	Alert,
} from "@mui/material";

import Tooltip, { tooltipClasses } from "@mui/material/Tooltip";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";
import TaskAltIcon from "@mui/icons-material/TaskAlt";
import WarningAmberIcon from "@mui/icons-material/WarningAmber";
import { styled } from "@mui/material/styles";

const HtmlTooltip = styled(({ className, ...props }) => (
	<Tooltip {...props} classes={{ popper: className }} />
))(({ theme }) => ({
	[`& .${tooltipClasses.tooltip}`]: {
		// backgroundColor: 'transparent',
		padding: 0,
		// overflow: 'hidden',
	},
}));

export const validateField = (props) => {
	const { rules, value, callback } = props;

	let errorMessage = null;
	let errorObject = {};

	if (value?.length < rules.minLength) {
		errorMessage = "Value is too short";
		errorObject["minLength"] = errorMessage;
	}
	if (value?.length > rules.maxLength) {
		errorMessage = "Value is too long";
		errorObject["maxLength"] = errorMessage;
	}

	// Email address validation
	// https://www.w3resource.com/javascript/form/email-validation.php
	if (rules.type === "email") {
		if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(value) == false) {
			errorMessage = "Not a valid email address";
			errorObject["email"] = errorMessage;
		}
	}

	if (rules.type === "username") {
		const regexObj = new RegExp(/^[a-zA-Z0-9 ._-]+$/);

		if (regexObj.test(value) == false) {
			errorMessage = "Invalid char(s)";
			errorObject["allowedChars"] = errorMessage;
		}
	}

	if (callback) {
		if (Object.entries(errorObject).length > 0) {
			callback(errorObject);
		} else {
			callback(null);
		}
	}
};

const TextfieldComponent = (props) => {
	const fieldData = props;
	const { validate, defaultValue, callback } = props;
	const propsOnChange = props.onChange;

	const [tooltip, setTooltip] = useState(false);
	const [errors, setErrors] = useState();

	const handleClick = (event) => {
		setTooltip(true);
	};

	const handleClose = () => {
		setTooltip(false);
	};

	const callbackHandler = useCallback(
		(event, errorObj) => {
			console.log(errorObj);
			setErrors(errorObj);

			// Merge error object into default event variable
			const callbackObj = { ...event, ...{ errors: errorObj } };

			// Fetch onChange func from props because we already use it in this sub component
			propsOnChange && propsOnChange(callbackObj); // eslint-disable-line
		},
		[propsOnChange]
	);

	const handleValidate = (event) => {
		if (validate.onChange) {
			validateField({
				rules: validate,
				value: event.target.value,
				callback: (errorObj) => callbackHandler(event, errorObj),
			});
		} else {
			callbackHandler(event, null);
		}
	};

	const onChange = (event) => {
		props.onChange && props.onChange(event);
	};

	useEffect(() => {
		console.log(props.validate);
		if (validate?.onInit) {
			// console.log(props)
			// Validate field
			console.log("revalidate");
			validateField({
				rules: validate,
				value: defaultValue,
				callback: (errorObj) => {
					console.log(errorObj);
					callbackHandler(
						{ target: { name: props.name, value: defaultValue } },
						errorObj
					);
				},
			});
		}
	}, [defaultValue, props.name, props.validate, validate, callbackHandler]);

	const fieldObj = <TextField {...props} type="text" onChange={onChange} />;

	return (
		<Box sx={{ display: "flex", width: "100%" }}>
			{!validate && fieldObj}

			{validate && (
				<HtmlTooltip
					// open={undefined}
					// open={(!errors && !validate.checklist) ? false : undefined}
					// onOpen={}
					// disableHoverListener
					// disableHoverListener={!errors && !validate.checklist}
					// disableFocusListener={!errors && !validate.checklist ? true : false}
					// disableTouchListener={!errors && !validate.checklist ? true : false}
					// disableTouchListener
					// onClose={handleClose}
					placement="bottom-end"
					arrow={Boolean(errors || validate.checklist)}
					title={
						(Boolean(errors) || Boolean(validate.checklist)) && (
							<Paper elevation={4} sx={{ p: 1 }}>
								<Stack spacing={1}>
									{validate.minLength && (
										<Stack
											spacing={2}
											direction="row"
											justifyContent="start"
											alignItems="start"
										>
											{errors?.minLength ? (
												<WarningAmberIcon color="warning" />
											) : (
												<TaskAltIcon color="success" />
											)}
											<Typography variant="caption">
												At least {validate.minLength} characters
											</Typography>
										</Stack>
									)}
									{validate.maxLength && (
										<Stack
											spacing={2}
											direction="row"
											justifyContent="start"
											alignItems="start"
										>
											{errors?.maxLength ? (
												<WarningAmberIcon color="warning" />
											) : (
												<TaskAltIcon color="success" />
											)}
											<Typography variant="caption">
												Maximum {validate.maxLength} characters
											</Typography>
										</Stack>
									)}
									{validate.allowedChars && (
										<Stack
											spacing={2}
											direction="row"
											justifyContent="start"
											alignItems="start"
										>
											{errors?.allowedChars ? (
												<WarningAmberIcon color="warning" />
											) : (
												<TaskAltIcon color="success" />
											)}
											<Typography variant="caption">
												Allowed characters:
												<br />
												<code>{validate.allowedChars.join(` | `)}</code>
											</Typography>
										</Stack>
									)}
									{errors?.email && (
										<Stack
											spacing={2}
											direction="row"
											justifyContent="start"
											alignItems="start"
										>
											<WarningAmberIcon color="warning" />
											<Typography variant="caption">{errors.email}</Typography>
										</Stack>
									)}
								</Stack>
							</Paper>
						)
					}
				>
					<TextField
						{...props}
						type="text"
						// onClick={handleClick}
						onChange={handleValidate}
						focused={Boolean(errors)}
						color={errors ? "warning" : "success"}
					/>
				</HtmlTooltip>
			)}
		</Box>
	);
};

export default TextfieldComponent;
