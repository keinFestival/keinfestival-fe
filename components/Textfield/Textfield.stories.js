import React from "react";
import TextField from 'components/Textfield/Textfield'

const defaultExport = {
  title: "Components/Inputs/TextField",
  component: TextField,
}

const Template = (args) => (
  <TextField {...args}/>
)

export const Default = Template.bind({})
Default.args = {
  name: "username",
  label: "Username",
  defaultValue: 'Ned Stark?',
  // onChange: (event) => onChange(event),
  variant: "outlined",
  fullWidth: true,
}

export const LiveValidation = Template.bind({})
LiveValidation.args = {
  name: "username",
  label: "Username",
  defaultValue: 'Ned Stark?',
  // onChange: (event) => onChange(event),
  variant: "outlined",
  fullWidth: true,
  validate: {
    type: 'username',
    minLength: 3,
    maxLength: 10,
    allowedChars: ['a-z', 'A-Z', '0-9', ' ', '_', '-'],
    checklist: true,
    onChange: true,
    onInit: true
  }
}

export default defaultExport