import React, { useState, useEffect, useContext, useCallback } from "react";
import { useRouter } from "next/router";
import NextLink from "next/link";
import Cookies from "js-cookie";

import { fetchAPI } from "/lib/api";
import AppContext from "/lib/context/AppContext";

import {
	Container,
	Grid,
	Box,
	Paper,
	Typography,
	Button,
	Link,
	TextField,
	Stack,
} from "@mui/material";

export default function Login() {
	const [data, updateData] = useState({ identifier: "", password: "" });
	const [loading, setLoading] = useState(false);
	const [error, setError] = useState(false);
	const router = useRouter();
	const appContext = useContext(AppContext);
	const {
		initContext,
		user,
		setUser,
		setShowLogin,
		setAuthenticated,
		isAuthenticated,
	} = useContext(AppContext);

	const onChange = (event) => {
		updateData({ ...data, [event.target.name]: event.target.value });
	};

	const submitHandler = useCallback(
		(data) => {
			setLoading(true);

			fetchAPI(
				"/auth/local",
				{},
				{
					body: JSON.stringify({
						identifier: data.identifier,
						password: data.password,
					}),
					method: "post",
				}
			)
				.then((res) => {
					setLoading(false);
					// set authed User in global context to update header/app state
					if (res.user) {
						Cookies.set("token", res.jwt);
						// appContext.setUser(res.user)
						const user = res.user;

						setUser({ data: user });
						setShowLogin(false);

						setTimeout(() => {
							setAuthenticated(true);
							router.push(router.query.redirect || "/user"); // redirect if you're already logged in
						}, 1000);
					} else {
						setError(false);
					}

					// create error object if api return error data in response
					if (res.error) {
						// console.log(res.error)
						const errorObject = {
							status: res.error.status,
							message: res.error.message,
							name: res.error.name,
							details: {},
						};

						res.error.details.errors.map((error) => {
							error.path.map((errPath) => {
								errorObject.details[error.path] = {
									message: error.message,
									name: error.name,
								};
							});
						});

						setError(errorObject);
					} else {
						setError(false);
					}
				})
				.catch((error) => {
					console.log(error);
					const errorObject = {
						message: "login data is not valid",
					};
					setError(errorObject);
					setLoading(false);
				});
		},
		[router, setShowLogin, setUser, setAuthenticated]
	);

	useEffect(() => {
		const listener = (event) => {
			if (event.code === "Enter" || event.code === "NumpadEnter") {
				event.preventDefault();
				submitHandler(data);
			}
		};
		document.addEventListener("keydown", listener);
		return () => {
			document.removeEventListener("keydown", listener);
		};
	}, [data, submitHandler]);

	const getError = (field) => {
		let isErr = false;

		// console.log('error 2: ', error)

		if (error == false) {
			isErr = false;
		} else {
			if (error.details?.[field]) {
				isErr = true;
			}

			// Set "Login-Error" for identifier and password field
			// If no error details are set it's because user is unknown
			if (
				(field === "identifier" || field == "password") &&
				error.details == null
			) {
				isErr = true;
			}
		}

		return isErr;
	};

	return (
		// <Container as="form" maxWidth="sm">
		// <Paper elevation={2} as="form">
		<Box
			component="fieldset"
			sx={{
				// px: 2,
				// py: 4,
				display: "flex",
				flexWrap: "wrap",
				flexDirection: "column",
				border: "none",
				p: 0,
			}}
			noValidate
			autoComplete="off"
			disabled={loading}
		>
			<Stack spacing={2}>
				<TextField
					error={getError("identifier")}
					helperText={error?.details?.identifier?.message}
					label="E-Mail"
					variant="outlined"
					name="identifier"
					onChange={(event) => onChange(event)}
				/>
				<TextField
					error={getError("password")}
					helperText={error?.details?.password?.message}
					label="Password"
					variant="outlined"
					name="password"
					type="password"
					onChange={(event) => onChange(event)}
				/>
			</Stack>

			{/* <Typography variant="h4" component="div" color="error">Error</Typography> */}

			{!error?.details && error?.message && (
				<Typography variant="subtitle2" component="div" color="error">
					{error.message}
				</Typography>
			)}

			{/* {error.details.identifier ? 'true' : 'false'} */}
			<Stack spacing={2} sx={{ pt: 4, width: "100%" }}>
				<Button
					fullWidth
					variant="contained"
					size="large"
					disabled={loading}
					onClick={() => {
						submitHandler(data);
					}}
				>
					Login
					{/* {loading ? "... " : "Login"} */}
				</Button>

				<Stack spacing={2} direction="row" justifyContent="space-between">
					<NextLink href={`/registration`} passHref>
						<Link color="gray" underline="hover">
							<Typography
								variant="caption"
								noWrap
								component="div"
								textAlign="right"
							>
								Don&lsquo;t have an account yet?
							</Typography>
						</Link>
					</NextLink>

					{/* <NextLink href={`/account/reset-password`} passHref>
						<Link color="gray" underline="hover">
							<Typography
								variant="caption"
								noWrap
								component="div"
								textAlign="right"
							>
								Forgot password?
							</Typography>
						</Link>
					</NextLink> */}
				</Stack>
			</Stack>
		</Box>
		// </Paper>
		// </Container>
	);
}
