import React from "react";

import Login from "./Login";

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
const defaultExport = {
	title: "Components/Forms/Login",
	component: Login,
	// More on argTypes: https://storybook.js.org/docs/react/api/argtypes
	argTypes: {
		// backgroundColor: { control: "color" },
	},
};

// // More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template = (args) => <Login {...args} />;

export const Default = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
Default.args = {};

export default defaultExport;
