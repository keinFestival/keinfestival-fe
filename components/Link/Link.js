import React, { useState, useEffect, useContext } from "react";
import { useRouter } from 'next/router'
import NextLink from "next/link"
import Cookies from "js-cookie";

import { fetchAPI } from "/lib/api";
import AppContext from "/lib/context/AppContext";

import {
  Link
  } from "@mui/material";

export default function LinkComponent(props) {
  const {underline, component, text, variant, onClick, children} = props;


  return (
    <Link {...props} />
  )
}