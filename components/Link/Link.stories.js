import React from "react";

import Link from "./Link";

const argTypes = {
	underline: {
		control: {
			type: "radio",
			options: ["none", "hover", "always"],
		},
	},
	variant: {
		control: {
			type: "radio",
			options: [null, "body2"], // body1 is default by MUI
		},
	},
	target: {
		control: {
			type: "select",
			options: [null, "_blank", "_self", "_parent", "_top"],
		},
	},
};

const defaultExport = {
	title: "Components/Navigation/Link",
	component: Link,
	argTypes: { ...argTypes },
};

// // More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template = (args) => <Link {...args}>This is a Link</Link>;

export const Default = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
Default.args = {
	underline: "none",
	variant: "none",
	href: "https://ecosia.de",
	target: "_blank",
	onClick: () => {
		console.log("Link Component");
	},
};

export default defaultExport;
