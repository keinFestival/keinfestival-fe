import React from "react"
import PropTypes from "prop-types"
// import "./button.css"

import {
  Button
} from "@mui/material";

// import MuiButton from '@mui/material/Button';

/**
 * Primary UI component for user interaction
 */
const ButtonComponent = (props) => {
  return (
    <>
      <Button {...props}/>
      {/* <MuiButton variant="contained" color="primary">Primary</MuiButton>
      <MuiButton variant="contained" color="secondary">Secondary</MuiButton>
      <MuiButton variant="outlined" disabled>
        Disabled
      </MuiButton>
      <MuiButton variant="outlined" href="#outlined-buttons">
        Link
      </MuiButton> */}
    </>
  )
}

ButtonComponent.propTypes = {
  variant: PropTypes.oneOf([null, "text", "contained", "outlined"]),
  size: PropTypes.oneOf([null, "small", "medium", "large"]),
  color: PropTypes.oneOf(["primary", "secondary", "info", "success", "error", "warning"]),
  component: PropTypes.oneOf([null, "a", "div", "span", "h2"]),
  /**
   * Button contents
   */
  label: PropTypes.string.isRequired,
  href: PropTypes.string,
  /**
   * Optional click handler
   */
  onClick: PropTypes.func,
  disabled: PropTypes.bool,
}

ButtonComponent.defaultProps = {
  onClick: undefined,
  disabled: undefined,
  variant: undefined,
  href: undefined,
  size: undefined,
  color: undefined,
  component: undefined,
  label: 'Button label'
}

export default ButtonComponent