import React, { useEffect, useState, useContext } from "react";
import { useQuery } from "@apollo/client";
// import { queryUsersPermissionsUser } from "/lib/queries.js";
// import { fetchAPI } from "/lib/api";
import AppContext from "/lib/context/AppContext";
import { gql } from "@apollo/client";
// import gql from "graphql-tag";
import {
	Box,
	Container,
	Typography,
	Stack,
	Paper,
	Grid,
	CircularProgress,
} from "@mui/material";

const queryUsersPermissionsUser = gql`
	query userDetails($id: ID!) {
		usersPermissionsUser(id: $id) {
			data {
				id
				attributes {
					firstName
					lastName
					username
					email
					confirmed
					blocked
					ticket_category {
						data {
							attributes {
								title
								price
							}
						}
					}
				}
			}
		}
	}
`;

const Ticketing = () => {
	const { user } = useContext(AppContext);
	const userId = user?.data?.id;

	// Get GraphQL data
	const { data, loading, error } = useQuery(queryUsersPermissionsUser, {
		variables: { id: userId },
	});

	console.log(data);

	return (
		<Stack spacing={2}>
			{loading ? (
				<Container
					sx={{
						py: 8,
						display: "flex",
						alignItems: "center",
						justifyContent: "center",
					}}
				>
					<CircularProgress color="primary" />
				</Container>
			) : data ? (
				<Stack spacing={0}>
					<Typography variant="subtitle1" fontWeight={600}>
						{
							data.usersPermissionsUser.data.attributes.ticket_category.data
								.attributes.title
						}
					</Typography>
					<Typography variant="body1">
						{
							data.usersPermissionsUser.data.attributes.ticket_category.data
								.attributes.price
						}
						,00 €
					</Typography>
				</Stack>
			) : (
				<Stack>
					<Typography variant="h5">Du hast noch kein Ticket.</Typography>
				</Stack>
			)}
		</Stack>
	);
};

export default Ticketing;
