import * as React from "react";
import { useRouter } from "next/router";
import NextLink from "next/link";

import { useContext, useEffect, useState } from "react";
import AppContext from "/lib/context/AppContext";
import { logout } from "/lib/auth";

import {
	Stack,
	AppBar,
	Box,
	Toolbar,
	IconButton,
	Typography,
	Menu,
	Container,
	Avatar,
	Tooltip,
	MenuItem,
	ListItemText,
	ListItemIcon,
} from "@mui/material";
import MuiLink from "@mui/material/Link";
import MenuIcon from "@mui/icons-material/Menu";
import LoginIcon from "@mui/icons-material/Login";
import LogoutIcon from "@mui/icons-material/Logout";
import SettingsIcon from "@mui/icons-material/Settings";

import { AnimatePresence, motion } from "framer-motion";

import ThemeSwitch from "/components/ThemeSwitch/ThemeSwitch";

const Header = ({ nav, show = null }) => {
	const router = useRouter();
	const [anchorElNav, setAnchorElNav] = useState(null);
	const [anchorElUser, setAnchorElUser] = useState(null);

	const appContext = useContext(AppContext);
	const { siteName, user, setUser, setShowLogin, setAuthenticated } =
		useContext(AppContext);

	const handleOpenNavMenu = (event) => {
		setAnchorElNav(event.currentTarget);
	};
	const handleCloseNavMenu = () => {
		setAnchorElNav(null);
	};

	const handleOpenUserMenu = (event) => {
		setAnchorElUser(event.currentTarget);
	};
	const handleCloseUserMenu = () => {
		setAnchorElUser(null);
	};

	const handleClickLogin = () => {
		setShowLogin(true);
	};

	const handleLogout = () => {
		setUser(null);
		setAuthenticated(false);
		logout();
		// handleCloseUserMenu();
	};

	const menuItemRoute = (event) => {
		event.preventDefault();
		handleCloseUserMenu();
		router.push(event.currentTarget.href);
	};

	const navVariants = {
		hidden: { opacity: 0, y: -10, height: 0 },
		enter: {
			opacity: 1,
			y: 0,
			transition: {
				delay: 0.25,
			},
		},
		exit: { opacity: 0, y: -10, height: 0 },
	};

	return (
		<Box component="header" sx={{ position: "sticky", top: 0 }}>
			<motion.div
				initial={!show && "hidden"}
				exit={!show && "exit"}
				animate={"enter"}
				variants={show !== false && navVariants}
				transition={{ type: "ease" }}
			>
				<AppBar
					// position="sticky"
					// color="secondary"
					// enableColorOnDark
					// style={{ background: "transparent" }}
					component="div"
				>
					{show && (
						<Container maxWidth="lg">
							<Toolbar disableGutters>
								<NextLink href={`/`} passHref scroll={false}>
									<MuiLink color="inherit" underline="none">
										<Typography
											variant="h6"
											component="div"
											sx={{
												flexGrow: 1,
												display: { xs: "none", md: "flex" },
												letterSpacing: "2pt",
											}}
										>
											<span style={{ fontWeight: 600 }}>kein</span>
											<span style={{ fontWeight: 200 }}>Festival</span>
										</Typography>
									</MuiLink>
								</NextLink>

								<Box sx={{ flexGrow: 1, display: { xs: "flex", md: "none" } }}>
									<IconButton
										size="large"
										aria-label="account of current user"
										aria-controls="menu-appbar"
										aria-haspopup="true"
										onClick={handleOpenNavMenu}
										color="inherit"
									>
										<MenuIcon />
									</IconButton>
									<Menu
										id="menu-appbar"
										anchorEl={anchorElNav}
										anchorOrigin={{
											vertical: "bottom",
											horizontal: "left",
										}}
										keepMounted
										transformOrigin={{
											vertical: "top",
											horizontal: "left",
										}}
										open={Boolean(anchorElNav)}
										onClose={handleCloseNavMenu}
										sx={{
											display: { xs: "block", md: "none" },
										}}
									>
										{nav?.map((item, index) => (
											<MenuItem key={index} onClick={handleCloseNavMenu}>
												<Typography textAlign="center">{item.title}</Typography>
											</MenuItem>
										))}
									</Menu>
								</Box>

								<Typography
									variant="h6"
									component="div"
									sx={{
										flexGrow: 1,
										display: { xs: "flex", md: "none" },
										letterSpacing: "2pt",
									}}
								>
									<span style={{ fontWeight: 600 }}>kein</span>
									<span style={{ fontWeight: 200 }}>Festival</span>
								</Typography>

								{/* Desktop Navigation */}
								<Box
									sx={{
										mr: 2,
										flexGrow: 1,
										display: { xs: "none", md: "flex" },
										justifyContent: "end",
									}}
								>
									{nav?.map((item, index) => (
										<NextLink
											href={`/${item.slug}`}
											passHref
											key={index}
											scroll={false}
										>
											<MenuItem
												color="inherit"
												underline="none"
												sx={{
													my: 2,
													mr: 2,
													color: "white",
													display: "block",
												}}
											>
												{item.title}
											</MenuItem>
										</NextLink>
									))}
								</Box>

								<Box sx={{ flexGrow: 0, display: "flex" }}>
									<Stack direction="row" spacing={2}>
										<ThemeSwitch />
										{/* {!user?.data && (
											<Tooltip
												title="Login"
												sx={{
													visibility: user?.loading ? "hidden" : "visible",
												}}
											>
												<IconButton
													aria-label="login"
													color="inherit"
													onClick={handleClickLogin}
												>
													<LoginIcon />
												</IconButton>
											</Tooltip>
										)} */}
										{user?.data && (
											<>
												<Tooltip title="Open settings" enterDelay={1000}>
													<IconButton
														id="appbar-menu-user-button"
														aria-controls={
															Boolean(anchorElUser)
																? "appbar-menu-user"
																: undefined
														}
														aria-haspopup="true"
														aria-expanded={
															Boolean(anchorElUser) ? "true" : undefined
														}
														onClick={handleOpenUserMenu}
														sx={{ p: 0 }}
													>
														{/* <Avatar alt="Remy Sharp" src="/static/images/avatar/2.jpg" /> */}
														<Avatar alt={user?.username} src="" />
													</IconButton>
												</Tooltip>
												<Menu
													sx={{ mt: "45px" }}
													id="appbar-menu-user"
													anchorEl={anchorElUser}
													anchorOrigin={{
														vertical: "top",
														horizontal: "right",
													}}
													// keepMounted
													transformOrigin={{
														vertical: "top",
														horizontal: "right",
													}}
													MenuListProps={{
														"aria-labelledby": "appbar-menu-user-button",
													}}
													open={Boolean(anchorElUser)}
													onClose={handleCloseUserMenu}
												>
													<MenuItem
														onClick={menuItemRoute}
														component="a"
														href="/user/account"
													>
														<SettingsIcon fontSize="small" />
														<Typography textAlign="center" sx={{ ml: 1 }}>
															Account
														</Typography>
													</MenuItem>

													<MenuItem onClick={handleLogout}>
														<LogoutIcon fontSize="small" />
														<Typography textAlign="center" sx={{ ml: 1 }}>
															Logout
														</Typography>
													</MenuItem>
												</Menu>
											</>
										)}
									</Stack>
								</Box>
							</Toolbar>
						</Container>
					)}
				</AppBar>
			</motion.div>
		</Box>
	);
};
export default Header;
