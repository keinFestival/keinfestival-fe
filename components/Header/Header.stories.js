import React from "react";
import Header from "./Header";

import { Box } from "@mui/material";
import { useContext } from "react";

import * as ImageStories from "/components/Image/Image.stories";

const defaultExport = {
	title: "Components/Layout/Header",
	component: Header,
};

// set backup default for isAuthenticated if none is provided in Provider

const Template = () => <Header />;

export const Default = Template.bind({});

export default defaultExport;
