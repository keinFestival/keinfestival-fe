// import React, { useState, useEffect } from 'react';
// import { storiesOf } from '@storybook/react';
// import { makeStyles } from '@material-ui/core/styles';
// import Container from '@material-ui/core/Container';
// import Typography from '@material-ui/core/Typography';
// import Grid from '@material-ui/core/Grid';
// import Box from '@material-ui/core/Box';
// import { white } from '@material-ui/core/colors';
// import { purple } from '@material-ui/core/colors';

// import Card from '@material-ui/core/Card';
// import CardContent from '@material-ui/core/CardContent';

// import { useTheme } from '@material-ui/core';
// // import { customTheme } from '../../../ThemeProvider';
// import customTheme from '../../../styles/MuiTheme';
// import { MuiThemeProvider, createTheme } from '@material-ui/core/styles';
// import addons from '@storybook/addons';

// const channel = addons.getChannel();

// const useStyles = makeStyles(theme => ({
//   colorBox: {
//     borderTop: '1px solid rgba(0,0,0, 0.125)',
//   },
// }));

// const isColorValue = value => {
//   return (
//     typeof value === 'string' &&
//     (value.startsWith('#') ||
//       value.startsWith('rgb') ||
//       value.startsWith('rgba'))
//   );
// };

// storiesOf('🎨 Styleguide', module).add('Colors', () => {
//   const [isDark, setDark] = useState(false);

//   useEffect(() => {
//     // listen to DARK_MODE event
//     channel.on('DARK_MODE', setDark);
//     return () => channel.off('DARK_MODE', setDark);
//   }, [channel, setDark]);

//   customTheme.palette.type = isDark ? 'dark' : 'light';

//   const theme = createTheme({
//     ...customTheme,
//   });

//   const classes = useStyles();
//   const palette = Object.keys(theme.palette);
//   let colorPaletteJSON = [];

//   palette.forEach((color, index) => {
//     let makeColorObject = {
//       name: color,
//       hues: [],
//     };

//     if (
//       typeof theme.palette[color] === 'object' ||
//       isColorValue(theme.palette[color]) === true
//     ) {
//       const colorObject = theme.palette[color];

//       if (!isColorValue(colorObject)) {
//         const hues = Object.keys(theme.palette[color]);

//         hues.forEach((hue, index) => {
//           const hueValue = colorObject[hue];
//           isColorValue(hueValue) &&
//             makeColorObject.hues.push({ name: hue, value: hueValue });
//         });
//       }
//       makeColorObject.hues.length > 0 && colorPaletteJSON.push(makeColorObject);
//     }
//   });

//   return (
//     <>
//       <Container
//         disableGutters={true}
//         maxWidth={false}
//         className={classes.container}
//       >
//         {/* <Typography variant="h5" component='h3' gutterBottom>Colors</Typography> */}
//         {colorPaletteJSON.map((color, index) => (
//           <Grid
//             container
//             spacing={1}
//             direction="row"
//             justify="flex-start"
//             alignItems="center"
//             className={classes.container}
//             key={index}
//           >
//             <Grid item xs={12}>
//               <Typography variant="h6" component="div">
//                 {color.name}
//               </Typography>
//             </Grid>
//             {color.hues.map((hue, index) => (
//               <Grid item xs={2} key={index}>
//                 <Card variant="outlined">
//                   <CardContent>
//                     <Grid container>
//                       <Grid item xs={12}>
//                         <Typography variant="subtitle2">{hue.name}</Typography>
//                       </Grid>
//                       <Grid item>
//                         <Typography color="textSecondary" variant="caption">
//                           {hue.value}
//                         </Typography>
//                       </Grid>
//                     </Grid>
//                   </CardContent>
//                   <CardContent
//                     style={{ background: hue.value }}
//                     className={classes.colorBox}
//                   ></CardContent>
//                 </Card>
//               </Grid>
//             ))}
//           </Grid>
//         ))}
//       </Container>
//     </>
//   );
// });
