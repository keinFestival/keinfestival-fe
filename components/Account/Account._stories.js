import Account from "components/Account/Account";
import { staticUserQuery } from "/lib/static/userQuery";

// set backup default for isAuthenticated if none is provided in Provider

const Template = (args) => <Account {...args} />;

export const Default = Template.bind({});

Default.args = {
	userQuery: {
		...staticUserQuery,
	},
};

const defaultExport = {
	title: "Components/Forms/Account",
	component: Account,
};

export default defaultExport;
