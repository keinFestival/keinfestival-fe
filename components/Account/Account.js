import React, { useEffect, useState, useContext, useCallback } from "react";
import Cookies from "js-cookie";
import { fetchAPI } from "/lib/api";
import AppContext from "/lib/context/AppContext";
import {
	Typography,
	Button,
	Grid,
	Box,
	Stack,
	Snackbar,
	Alert,
	FormControl,
	FormGroup,
	FormHelperText,
	FormControlLabel,
	Checkbox,
} from "@mui/material";

import InputField from "/components/Textfield/Textfield";

const Account = ({ user }) => {
	const enableSubmit = false;

	const { data, loading, error } = user;
	const appContext = useContext(AppContext);
	const userId = data?.usersPermissionsUser.data.id;

	const [formFields, updateFormFields] = useState(null);
	const [newData, updateNewData] = useState();
	const [formErrors, setFormErrors] = useState();
	const [storable, setStorable] = useState(enableSubmit || false);
	const [snackbar, setsnackbar] = useState({
		open: false,
		vertical: "bottom",
		horizontal: "center",
		handleClose: () => {
			setsnackbar({ ...snackbar, open: false });
		},
	});

	useEffect(() => {
		const userFields = data?.usersPermissionsUser?.data?.attributes;

		if (data) {
			const initFormFields = {
				username: {
					name: "username",
					label: "Username",
					defaultValue: userFields?.username,
					type: "text",
					variant: "outlined",
					fullWidth: true,
					validate: {
						type: "username",
						minLength: 3,
						maxLength: 10,
						allowedChars: ["a-z", "A-Z", "0-9", "_", "-", " "],
						checklist: true,
						onChange: true,
					},
				},
				email: {
					name: "email",
					label: "E-Mail",
					defaultValue: userFields?.email,
					type: "text",
					variant: "outlined",
					fullWidth: true,
					validate: {
						type: "email",
						onChange: true,
					},
				},
				// favJamStack: {
				//   name: "favJamStack",
				//   label: "E-Mail",
				//   defaultValue: userFields?.email,
				//   type: "checkbox",
				//   variant: "outlined",
				//   fullWidth: true,
				//   validate: {
				//     type: 'checkbox',
				//   }
				// },
			};

			updateFormFields({ ...initFormFields });
		}
	}, [data]);

	useEffect(() => {
		if (formErrors && Object.keys(formErrors).length === 0) {
			setFormErrors(null);
			setStorable(true);
		}
	}, [formErrors]);

	const token = Cookies.get("token");

	const onChangeFunc = (event) => {
		const inputKey = event.target.name;
		const inputValue = event.target.value;
		const inputErrors = event.errors;

		updateNewData({ ...newData, ...{ [inputKey]: inputValue } });

		if (inputErrors) {
			setFormErrors({ ...formErrors, [inputKey]: inputErrors });
			setStorable(false);
		} else {
			let newFormErrors = formErrors;
			delete newFormErrors?.[inputKey];
			setFormErrors({ ...newFormErrors });
		}
	};

	// console.log(data);
	// console.log(newData);

	// console.log('Object.keys(formErrors).length: ', formErrors && Object.keys(formErrors).length)
	// console.log('formErrors: ', formErrors)
	// console.log('formErrors: ', formErrors)

	const submitHandler = useCallback(
		(newData) => {
			console.log(formFields);

			let tempFormFields = { ...formFields };

			newData &&
				Object.keys(formFields).map((key, index) => {
					newData[key] && (tempFormFields[key].defaultValue = newData[key]);
					tempFormFields[key].validate.onInit = true;
				});

			// @TODO: Fix error when updating fields and validating
			// updateFormFields({ ...tempFormFields });

			fetchAPI(
				`/users/${userId}`,
				{},
				{
					headers: {
						Authorization: `Bearer ${token}`,
						"Content-Type": "application/json",
					},
					body: JSON.stringify(newData),
					method: "PUT",
				}
			)
				.then((res) => {
					if (!res.error) {
						appContext.setUser({ ...appContext.user, ...newData });
						setsnackbar({ ...snackbar, open: true });
						setStorable(false);
					}
				})
				.catch((err) => {
					console.log(err);
				});
		},
		[formFields, appContext, snackbar, token, userId]
	);

	useEffect(() => {
		const listener = (event) => {
			if (event.code === "Enter" || event.code === "NumpadEnter") {
				event.preventDefault();
				storable && submitHandler(newData);
			}
		};
		document.addEventListener("keydown", listener);
		return () => {
			document.removeEventListener("keydown", listener);
		};
	}, [storable, newData, submitHandler]);

	return (
		<Box sx={{ my: 4 }}>
			<Typography variant="h4" component="div">
				Account Component
			</Typography>

			<Box sx={{ my: 4 }}>
				{loading && "Loading..."}
				{error && "Error!"}
				{data && (
					<>
						<Grid
							container
							spacing={3}
							component="form"
							noValidate
							autoComplete="off"
						>
							{formFields &&
								Object.keys(formFields).map((key, index) => {
									const field = formFields[key];

									return (
										<Grid item xs={12} key={index}>
											{field.type === "text" && !loading && (
												<InputField {...field} onChange={onChangeFunc} />
											)}
											{field.type === "checkbox" && !loading && (
												<FormControl
													required
													error={"asd"}
													component="fieldset"
													variant="standard"
												>
													<FormGroup>
														<FormControlLabel
															control={<Checkbox />}
															label={
																<Stack direction="row" spacing={0.5}>
																	<Typography color="error">
																		I like this JAM Stack
																	</Typography>
																	<Typography color="red"> *</Typography>
																</Stack>
															}
														/>
													</FormGroup>
													<FormHelperText>Field is required</FormHelperText>
												</FormControl>
											)}
										</Grid>
									);
								})}

							<Grid item xs={12}>
								<Button
									disabled={!storable && !enableSubmit}
									color="success"
									variant="contained"
									size="large"
									sx={{ mt: 1 }}
									onClick={() => {
										submitHandler(newData);
									}}
								>
									Save changes (WIP)
								</Button>
							</Grid>
						</Grid>
						<Snackbar
							anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
							open={snackbar.open}
							onClose={snackbar.handleClose}
							message="Your changes were saved."
							autoHideDuration={6000}
						>
							<Alert
								onClose={snackbar.handleClose}
								variant="filled"
								severity="success"
								sx={{ width: "100%" }}
							>
								Your changes were successfully saved!
							</Alert>
						</Snackbar>
					</>
				)}
			</Box>
		</Box>
	);
};

export default Account;
