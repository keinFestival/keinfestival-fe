import {
	Container,
	Box,
	Stack,
	TextField,
	Paper,
	Typography,
	Button,
	Link,
	CircularProgress,
} from "@mui/material";

const PageLoader = () => {
	return (
		<Container
			sx={{
				height: "100vh",
				display: "flex",
				alignItems: "center",
				justifyContent: "center",
			}}
		>
			<CircularProgress color="primary" />
		</Container>
	);
};

export default PageLoader;
