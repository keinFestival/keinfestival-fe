import React, { useState, useContext, useEffect, useCallback } from "react";
import { useRouter } from "next/router";
import NextLink from "next/link";

import { registerUser } from "/lib/auth";
import AppContext from "/lib/context/AppContext";

import {
	Container,
	Stack,
	Box,
	TextField,
	Paper,
	Typography,
	Button,
	Link,
	InputAdornment,
} from "@mui/material";
import AccountCircle from "@mui/icons-material/AccountCircle";
import Cookies from "js-cookie";

import Layout from "/components/layout";

const inputVariant = "outlined";
const passCodeString = "Wilde Wiese";

const PassCode = ({ global, props }) => {
	const router = useRouter();
	const { setUser } = useContext(AppContext);
	const [data, setData] = useState({
		passcode: "",
	});
	const [loading, setLoading] = useState(false);
	const [error, setError] = useState({});
	const [validation, setValidation] = useState({});

	useEffect(() => {
		let temp = {};

		error?.error?.details?.errors?.forEach((error, index) => {
			console.log(index, error);
			console.log(error.path[0]);

			temp = {
				...temp,
				[error.path[0]]: { ...error },
			};
		});

		setValidation({
			...temp,
		});
	}, [loading, error?.error?.details?.errors]);

	useEffect(() => {
		const listener = (event) => {
			if (event.code === "Enter" || event.code === "NumpadEnter") {
				event.preventDefault();
				submitHandler(data);
			}
		};
		document.addEventListener("keydown", listener);
		return () => {
			document.removeEventListener("keydown", listener);
		};
	}, [data, submitHandler]);

	const submitHandler = useCallback((data) => {
		if (data.passcode === passCodeString) {
			Cookies.set("passcode", passCodeString);
			if (data.passcode === passCodeString) {
				router.push(router.query.redirect || "/registration");
			}
		} else {
			setValidation({ passCode: true });
		}
	});

	return (
		<Paper elevation={0}>
			<form>
				<Box
					// component="fieldset"
					sx={{
						display: "flex",
						flexWrap: "wrap",
						flexDirection: "column",
						border: "none",
						p: 0,
					}}
					noValidate
					autoComplete="off"
					disabled={loading}
				>
					<Typography variant="p" component="div" sx={{ mb: 0 }}></Typography>

					<Stack spacing={2} direction="row" sx={{ mb: 0 }}>
						<TextField
							error={Boolean(validation.passCode)}
							helperText={
								Boolean(validation.passCode) && "Wrong Access Code. Try again."
							}
							label="Access code"
							value={data.passCode}
							type="text"
							name="passcode"
							variant={inputVariant}
							disabled={loading}
							fullWidth
							onChange={(e) => setData({ ...data, passcode: e.target.value })}
						/>
					</Stack>
					<Box sx={{ pt: 4, width: "100%" }}>
						{error?.error && (
							<div style={{ marginBottom: 10 }}>
								<small style={{ color: "red" }}>{error.error.message}</small>
							</div>
						)}
						<Button
							variant="contained"
							size="large"
							disabled={loading}
							fullWidth
							onClick={() => submitHandler()}
						>
							{loading ? "Loading.." : "Los geht's"}
						</Button>
					</Box>
				</Box>
			</form>
		</Paper>
	);
};

export default PassCode;
