import { getStrapiMedia } from "/lib/media";
import NextImage from "next/image";

const Image = ({ image }) => {
	const ratio = image.ratio?.split(":");
	const ratioHeight = ratio && (image.width / ratio[0]) * ratio[1];

	return (
		<div className="image">
			<NextImage
				alt={image.alternativeText || " "}
				width={image.width}
				height={ratioHeight || image.height}
				src={getStrapiMedia(image)}
				objectFit={image.objectFit}
				layout="responsive"
				// loader="default"
				unoptimized={true}
				// loading="eager"
				// layout="fixed"
			/>
		</div>
	);
};

export default Image;
