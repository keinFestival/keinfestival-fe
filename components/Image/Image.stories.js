import React from "react";
import Image from "./Image";

import { Box } from "@mui/material";

const argTypes = {
	objectFit: {
		default: "cover",
		control: {
			type: "radio",
			options: ["unset", "cover", "contain"],
		},
	},
	ratio: {
		default: "original",
		control: {
			type: "radio",
			options: ["original", "1:1", "4:3", "16:9", "21:9"],
		},
	},
};

const defaultExport = {
	title: "Components/Media/Image",
	component: Image,
	argTypes: { ...argTypes },
};

const Template = (args) => (
	<Box sx={{ maxWidth: 400 }}>
		<Image image={args} alt="" />
	</Box>
);

export const Default = Template.bind({});

Default.args = {
	name: "the-internet-s-own-boy.jpg",
	alternativeText: "Alt text",
	url: "http://via.placeholder.com/800x600",
	// url: "/uploads/we_love_pizza_d1d8a4ad02.jpg",
	ratio: "original",
	objectFit: "cover",
	width: 800,
	height: 600,
};

export default defaultExport;
