import { Container, Box, AppBar, Grid, Typography } from "@mui/material";
import { AnimatePresence, motion } from "framer-motion";

const footerAnimations = {
	hidden: { opacity: 0, x: 0, y: 10, height: 0 },
	enter: {
		opacity: 1,
		x: 0,
		y: 0,
		scale: 1,
		transition: {
			delay: 0.25,
		},
	},
	exit: { opacity: 0, x: 0, y: 10, height: 0 },
};

const Footer = ({ show = true }) => {
	return (
		<Box component="footer">
			<motion.div
				initial="hidden"
				exit="exit"
				animate={show && "enter"}
				variants={footerAnimations}
				transition={{ type: "ease" }}
			>
				<AppBar position="relative" component="div">
					{show && (
						<Container maxWidth="lg" sx={{ py: 8 }}>
							<Grid container>
								<Grid item>
									<Typography variant="body2">
										Made with ♥, powered by Strapi CMS, Next.js and Material UI
									</Typography>
								</Grid>
							</Grid>
						</Container>
					)}
				</AppBar>
			</motion.div>
		</Box>
	);
};

export default Footer;
