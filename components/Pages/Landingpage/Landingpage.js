import React, { useState, useContext } from "react";
import NextLink from "next/link";
import { useRouter } from "next/router";

import { registerUser } from "/lib/auth";
import AppContext from "/lib/context/AppContext";

import {
	Container,
	Box,
	Stack,
	TextField,
	Paper,
	Typography,
	Button,
	Link,
} from "@mui/material";

import HowToRegIcon from "@mui/icons-material/HowToReg";
import LoginIcon from "@mui/icons-material/Login";

import Layout from "/components/layout";

const Landingpage = ({ global }) => {
	const router = useRouter();

	return (
		<Container
			maxWidth="sm"
			sx={{
				py: 4,
				px: 4,
			}}
		>
			<Box
				sx={{
					p: 4,
					border: "10px solid",
					borderColor: "primary.main",
					aspectRatio: "1",
					display: "flex",
					flexDirection: "column",
					justifyContent: "end",
				}}
			>
				<Typography
					variant="h2"
					fontWeight="bold"
					sx={{ lineHeight: 0.9, letterSpacing: "3pt" }}
				>
					kein
				</Typography>
				<Typography
					variant="h2"
					fontWeight="light"
					sx={{ lineHeight: 0.9, letterSpacing: "3pt", ml: "-.75%" }}
				>
					Festival
				</Typography>
				<Typography
					variant="small"
					fontWeight="light"
					sx={{ lineHeight: 1.25, letterSpacing: "3pt", mt: 3 }}
				>
					2022_07_15-17
				</Typography>
			</Box>
			<Stack
				spacing={1.6}
				direction="row"
				justifyContent="space-between"
				sx={{ pt: 1.6 }}
			>
				<Button
					variant="contained"
					fullWidth
					sx={{ fontWeight: 600 }}
					size="large"
					color="primary"
					onClick={() => {
						router.push("/registration");
					}}
				>
					Register
				</Button>
				<Button
					variant="outlined"
					color="primary"
					size="large"
					// endIcon={"🔒"}
					// endIcon={<LoginIcon />}
					onClick={() => {
						router.push("/login");
					}}
				>
					Login
				</Button>
			</Stack>
		</Container>
	);
};

export default Landingpage;
