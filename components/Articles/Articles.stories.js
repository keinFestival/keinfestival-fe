import React from "react";
import Articles from "./Articles";

import { Box } from "@mui/material";
import * as CardStories from "/components/Card/Card.stories";

const defaultExport = {
	title: "Components/Articles",
	component: Articles,
	argTypes: {
		...CardStories.default.argTypes,
	},
};

const Template = (args) => {
	return (
		<Box sx={{}}>
			<Articles {...args} />
		</Box>
	);
};

export const Default = Template.bind({});

Default.args = {
	grid: 3,
	url: "http://via.placeholder.com/1000x600",
	articles: [
		{ attributes: { ...CardStories.Default.args } },
		{ attributes: { ...CardStories.Default.args } },
		{ attributes: { ...CardStories.Default.args } },
		{ attributes: { ...CardStories.Default.args } },
		{ attributes: { ...CardStories.Default.args } },
		{ attributes: { ...CardStories.Default.args } },
		{ attributes: { ...CardStories.Default.args } },
		{ attributes: { ...CardStories.Default.args } },
		{ attributes: { ...CardStories.Default.args } },
		{ attributes: { ...CardStories.Default.args } },
		{ attributes: { ...CardStories.Default.args } },
		{ attributes: { ...CardStories.Default.args } },
	],
};

export default defaultExport;
