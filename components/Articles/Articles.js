import React from "react";
import { Grid } from "@mui/material";
import Card from "/components/Card/Card";

const Articles = ({ articles, grid, objectFit, ratio }) => {
	return (
		<Grid container spacing={2}>
			{articles.map((article, index) => {
				article.attributes.objectFit = objectFit;
				article.attributes.ratio = ratio;

				return (
					<Grid item xs={Number(grid) || 4} key={index}>
						<Card
							article={article}
							index={index}
							key={`article__${article.slug}`}
						/>
					</Grid>
				);
			})}
		</Grid>
	);
};

export default Articles;
