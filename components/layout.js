// React
import React, { useContext, useState, useEffect } from "react";
// GraphQL / Apollo
// import { useQuery } from "@apollo/client";
// import { queryUserMe, queryUsersPermissionsUser } from "/lib/queries.js";
// App
import AppContext from "/lib/context/AppContext";
// Framer Motion
import { AnimatePresence, motion } from "framer-motion";
// Material UI
import {
	Box,
	Stack,
	Modal,
	Paper,
	Container,
	Fade,
	Zoom,
	Grow,
	Slide,
	Dialog,
} from "@mui/material";
import Header from "/components/Header/Header";
import Footer from "/components/Footer/Footer";
import Login from "/components/Login/Login";

const variants = {
	hidden: { opacity: 0, x: 0, y: 10 },
	enter: {
		opacity: 1,
		x: 0,
		y: 0,
		scale: 1,
	},
	exit: { opacity: 0, x: 0, y: 10 },
};

const Layout = ({ children, nav, categories, seo, type = null }) => {
	const { showLogin, setShowLogin, setUser, user } = useContext(AppContext);
	const [navVisibility, setNavVisibility] = useState(false);
	const handleClose = () => setShowLogin(false);

	// const userMe = useQuery(queryUserMe);

	const userId = user?.data?.id;

	// Get GraphQL data
	// const userMe = useQuery(queryUsersPermissionsUser, {
	// 	variables: { id: userId },
	// });

	useEffect(() => {
		setNavVisibility(type !== "blank");
		if ((type !== "blank") !== navVisibility) {
			// console.log(navVisibility);
		}
	}, [type, navVisibility]);

	return (
		<Box>
			<Stack>
				{/* Header */}
				<Header nav={nav} show={navVisibility} />

				{/* Main content */}
				<Box
					sx={{
						flexGrow: 1,
						py: 8,
						display: "flex",
						flexDirection: "column",
						justifyContent: type === "blank" ? "center" : "start",
						minHeight: "100vh",
					}}
				>
					<motion.div
						initial="hidden"
						animate="enter"
						exit="exit"
						variants={variants}
						transition={{ type: "ease" }}
					>
						{children}
					</motion.div>
				</Box>
			</Stack>

			{/* Footer */}
			<Footer show={type !== "blank"} />

			<Dialog
				open={showLogin}
				keepMounted
				onClose={handleClose}
				aria-labelledby="modal-login"
				aria-describedby="modal-modal-description"
			>
				<Box width={400}>
					<Paper elevation={4}>
						<Login />
					</Paper>
				</Box>
			</Dialog>
		</Box>
	);
};

export default Layout;
